package com.zf.states
{
	public interface IZFState
	{
		function update():void;
		function destroy():void;
	}
}